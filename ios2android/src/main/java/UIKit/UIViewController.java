package UIKit;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import core.iOS;

public class UIViewController extends AppCompatActivity implements iOS {

    final Integer _layoutId;
    public UIViewController() {
        _layoutId=null;
    }

    public int layoutId() {
        return _layoutId;
    }

    public UIViewController(Integer layoutId) {
        super();
        this._layoutId = layoutId;
    }

    public UIViewController(String nibName, Bundle bundle) {
        super();
        this._layoutId = getResources().getIdentifier(fix(nibName), "layout", getPackageName());
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getIntent().getIntExtra("OneKit|layoutId", 0));
    }
}
