package UIKit;

public class UINavigationItem {
    private final String _title;

    public UINavigationItem(String title) {
        _title = title;
    }

    public String title() {
        return _title;
    }
}
