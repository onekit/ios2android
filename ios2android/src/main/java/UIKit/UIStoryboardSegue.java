package UIKit;

import android.app.Activity;

import core.ACTION;

public class UIStoryboardSegue {
    private final ACTION _performHandler;
    private final Activity _source;
    private final Activity _destination;
    private final String _identifier;

    public UIStoryboardSegue(String identifier, Activity source, Activity destination, ACTION performHandler) {
        _identifier = identifier;
        _source = source;
        _destination = destination;
        _performHandler = performHandler;
    }

    public UIStoryboardSegue(String identifier, Activity source, Activity destination) {
        this(identifier, source, destination, null);
    }

    public String identifier() {
        return _identifier;
    }

    public Activity sourceViewController() {
        return _source;
    }

    public Activity destinationViewController() {
        return _destination;
    }

    public void perform() {

    }
}
