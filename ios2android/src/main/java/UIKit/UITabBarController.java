package UIKit;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class UITabBarController extends UIViewController {
    private final List<UIViewController> _viewControllers=new ArrayList<>();
    public UITabBarController(Integer layoutId) {
        super(layoutId);
    }

    public UITabBarController(String nibName, Bundle bundle) {
        super(nibName,bundle);
    }

    public List<? extends UIViewController>  viewControllers() {
        return _viewControllers;
    }
}
