package UIKit;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

public class UINavigationController  extends UIViewController {
    private final List<UIViewController> _viewControllers=new ArrayList<>();
    private final UIViewController _rootViewController;

    public UINavigationController(UIViewController rootViewController) {
        _rootViewController = rootViewController;
        _viewControllers.add(_rootViewController);
    }

    public <T extends UIViewController> T rootViewController() {
        return (T) _rootViewController;
    }
    public List<? extends UIViewController>  viewControllers() {
        return _viewControllers;
    }
    public void pushViewController_animated(UIViewController viewController ,Boolean animated){
        _viewControllers.add(viewController);
        startActivity(new Intent(this,viewController.getClass()));
    }
}
