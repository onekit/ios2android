package UIKit;

import android.app.Application;
import android.content.Context;

public abstract class UIApplication extends Application {
    static UIApplication app;

    public UIApplication() {
        app = this;
    }
}
