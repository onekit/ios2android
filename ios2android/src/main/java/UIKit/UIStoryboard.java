package UIKit;

import android.content.Context;
import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

public abstract class UIStoryboard {


    abstract protected int initialViewController();

    abstract protected List<Class<? extends UIViewController>> modules();

    abstract protected List<UINavigationItem> navigationItmes();
    abstract protected List<UITabBarItem> tabBarItems();

    abstract protected Map<String, Integer> identifiers();

    public <T extends UIViewController> T instantiateInitialViewController() {
        int index = initialViewController();
        return _instantiateViewController(index);
    }

    private <T extends UIViewController> T _instantiateViewController(int index) {
        Class<? extends UIViewController> clazz = modules().get(index);

        try {
            Context context = UIApplication.app;
            Constructor<? extends UIViewController> constructor = clazz.getConstructor(Integer.class);
            int layoutId = context.getResources().getIdentifier(String.format("%s_storyboard_%d", getClass().getSimpleName().toLowerCase(), index), "layout", context.getPackageName());
            return (T) constructor.newInstance(layoutId);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public <T extends UIViewController> T instantiateViewController(String withIdentifier) {
        int index = identifiers().get(withIdentifier);
        return _instantiateViewController(index);
    }
}
