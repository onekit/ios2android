package UIKit;

import android.graphics.drawable.Drawable;

public class UIBarItem {
    private final String _title;
    private final Drawable _image;

    public UIBarItem(String title, Drawable image) {
        _title = title;
        _image = image;
    }

    public String title() {
        return _title;
    }
    public Drawable image() {
        return _image;
    }
}
