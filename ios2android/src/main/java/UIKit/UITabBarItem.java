package UIKit;

import android.graphics.drawable.Drawable;

public class UITabBarItem extends UIBarItem{
    public UITabBarItem(String title, Drawable image) {
        super(title, image);
    }
}
