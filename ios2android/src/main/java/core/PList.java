package core;

import android.content.Context;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class PList {
    private final Context context;

    public PList(Context context) {
        this.context = context;
    }

    private Object doNode(Element element) {
        switch (element.getNodeName()) {
            case "dict": {
                Map<String, Object> map = new HashMap<>();
                NodeList children = element.getChildNodes();
                String key=null;
                for (int i = 0; i < children.getLength(); i ++) {
                    Node child = children.item(i);
                    if (!(child instanceof Element)) {
                        continue;
                    }
                    if (child.getNodeName().equals("key")) {
                        key = child.getTextContent();
                    } else {
                        Object value = doNode((Element) child);
                        map.put(key, value);
                    }
                }
                return map;
            }
            case "array": {
                List<Object> list = new ArrayList<>();
                NodeList children = element.getChildNodes();
                for (int i = 0; i < children.getLength(); i++) {
                    Node child = children.item(i);
                    if(!(child instanceof Element)){
                        continue;
                    }
                    list.add(doNode((Element) children.item(i)));
                }
                return list;
            }
            case "string":
                return element.getTextContent();
            default:
                return null;
        }
    }

Element getChild(Element element,int index) {
    NodeList children = element.getChildNodes();
    int idx=0;
    for (int i = 0; i < children.getLength(); i++) {
        Node child = children.item(i);
        if(!(child instanceof Element)){
            continue;
        }
        if(index==idx){
            return (Element) child;
        }
        idx++;
    }
    return null;
}
    public Map<String, Object> load(String fileName) {
        try {
            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
            Document document = docBuilder.parse(context.getAssets().open(fileName));
            Element root = document.getDocumentElement();
            return (Map<String, Object> )doNode(getChild(root,0));
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
