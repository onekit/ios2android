package core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import java.util.Map;

import UIKit.UIStoryboard;
import UIKit.UIViewController;

public class Launch extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Map<String, Object> plist = new PList(this).load("Info.plist");
        String storyboardName = "Main";
        try {
            Class<UIStoryboard> clazz = (Class<UIStoryboard>) Class.forName(String.format("%s.storyboard.%s", getPackageName(), storyboardName));
            UIStoryboard storyboard = clazz.newInstance();
            UIViewController initialViewController = storyboard.instantiateInitialViewController();
            Intent intent = new Intent(this, initialViewController.getClass());
            intent.putExtra("OneKit|layoutId", initialViewController.layoutId());
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
