package core;

public abstract class FUNC4<T1,T2,T3,T4,TResult> {
    public  abstract TResult invoke(T1 arg1,T2 arg2,T3 arg3,T4 arg4);
}
