package core;

public abstract class FUNC<TResult> {
    public  abstract TResult invoke();
}
