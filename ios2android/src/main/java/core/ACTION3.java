package core;

public abstract class ACTION3<T1,T2,T3> {
    public  abstract void invoke(T1 arg1,T2 arg2,T3 arg3);
}
