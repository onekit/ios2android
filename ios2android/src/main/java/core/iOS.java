package core;

public interface iOS {
    default String fix(String name) {
        return OneKit.fix(name);
    }
}
