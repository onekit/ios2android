package core;

public abstract class ACTION4<T1,T2,T3,T4> {
    public  abstract void invoke(T1 arg1,T2 arg2,T3 arg3,T4 arg4);
}
