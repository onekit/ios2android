package demo.app;

import android.os.Bundle;

import UIKit.UIViewController;

public class ViewControllerA extends UIViewController {
    public ViewControllerA( ) {
        super();
    }
    public ViewControllerA(Integer coder) {
        super(coder);
    }

    public ViewControllerA(String nibName, Bundle bundle) {
        super(nibName, bundle);
    }
}