package demo.app.storyboard;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import UIKit.UINavigationItem;
import UIKit.UIStoryboard;
import UIKit.UITabBarItem;
import UIKit.UIViewController;
import demo.app.ViewControllerA;

public class Main extends UIStoryboard {


    @Override
    protected int initialViewController() {
        return 0;
    }
    @Override
    protected List< Class<? extends UIViewController>> modules() {
        return new ArrayList< Class<? extends UIViewController>>(){{
            add(ViewControllerA.class);
        }};
    }
    @Override
    protected List<UINavigationItem> navigationItmes() {
        return new ArrayList<UINavigationItem>(){{
            add(new UINavigationItem("VC1"));
        }};
    }

    @Override
    protected List<UITabBarItem> tabBarItems() {
        return new ArrayList<UITabBarItem>(){{
            add(new UITabBarItem("VC1",null));
        }};
    }

    @Override
    protected Map<String, Integer> identifiers() {
        return new HashMap<String, Integer>(){{

        }};
    }
}
